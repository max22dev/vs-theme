/**
 * Delay calling the target method until the debounce period has elapsed with no additional debounce call
 *
 * @param {object} context;
 * @param {function} func - The method to invoke.
 * @param {number} delay - Number of milliseconds to wait
 * @returns {function(...[*]=)}
 */

export default function debounce (func, delay) {
  let timeout;
  return function (...args) {
    if (timeout) {
      clearTimeout(timeout);
    }
    timeout = setTimeout(() => { timeout = null; func.apply(this, args); }, delay);
  };
}
